import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { finalize, Observable, tap } from 'rxjs';
import { Trainer } from '../models/trainer.model';
import { TrainerService } from './trainer.service';

const { apiPokemon, apiKey, apiTrainers } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private _pokemonList: Pokemon[] = [];
  private _loading: boolean = false;

  get pokemonList(): Pokemon[] {
    return this._pokemonList;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor( private readonly http: HttpClient, private readonly trainerService: TrainerService ) { }

  public findAllPokemon() {
    if ( this.pokemonList.length > 0 || this.loading) {
      return
    }
    this._loading = true;
    this.http.get(`${apiPokemon}/?limit=5`)
    .pipe(
      finalize(() => {
        this._loading = false 
      })
    )
    .subscribe((response: any) => {
      response.results.forEach((result: { name: string; }) => {
        this.getMooreData(result.name)
          .subscribe((pokemon: Pokemon) => {
            this._pokemonList.push(pokemon)
          })
      })
    });
  }

  public getMooreData(name: string) {
    return this.http.get<Pokemon>(`${apiPokemon}/${name}`)
  }

  public pokemonById(id: number): Pokemon | undefined {
    return this._pokemonList.find((pokemon: Pokemon) => pokemon.id === id)
  }


  public addToCaught(pokemonId: number): Observable<Trainer> {
    if(!this.trainerService.trainer) {
      throw new Error('addToCaught: There is no Trainer')
    }

    const trainer:Trainer = this.trainerService.trainer

    const pokemon: Pokemon | undefined = this.pokemonById(Number(pokemonId))
    if(!pokemon) {
      throw new Error('addToCaught: No Pokemon with id: ' + pokemonId)
    }

    if(this.trainerService.inFavourites(Number(pokemonId))) {
      this.trainerService.removeFromFavourites(Number(pokemonId))
    } else {
      this.trainerService.addToFavourites(pokemon)
    }

    const headers = new HttpHeaders({
      'content-type':'application/json',
      'x-api-key': apiKey
    })

    return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`, {
      pokemon: [...trainer.pokemon]
    }, {headers})
      .pipe(
        tap((updatedTrainer:Trainer)=>{
          this.trainerService.trainer = updatedTrainer
        })
      )
    
  }

}
