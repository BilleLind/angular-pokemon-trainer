import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { DetailsPage } from './pages/details/details.page';
import { LoginPage } from './pages/login/login.page';
import { PageNotFoundPage } from './pages/page-not-found/page-not-found.page';
import { TrainerPage } from './pages/trainer/trainer.page';

const routes: Routes = [

    { path: 'login', component: LoginPage, }, 

    // routes protected by auth guard
    { path: '', pathMatch: 'full', redirectTo: "/catalogue" },
    { path: 'catalogue', component: CataloguePage, canActivate:[AuthGuard] },

    { path: 'trainer', component: TrainerPage, canActivate:[AuthGuard] },

    { path: 'detail/:id', component: DetailsPage, canActivate:[AuthGuard] },

    // otherwise redirect to PageNotFoundPage
    { path: '**', component: PageNotFoundPage }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}