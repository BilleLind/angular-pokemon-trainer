import { Pipe, PipeTransform } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';

@Pipe({
  name: 'searchFilter',
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: string, queryType: string): Pokemon[] {
    if (!items) return [];

    if (!searchText) return items;

    searchText = searchText.toLowerCase();

    if (queryType === 'name') {
      return items.filter((it) => {
        return it.name.toString().toLowerCase().includes(searchText);
      });
    }
    console.log(items[0].types[0].type.name)
    if (queryType === 'type') {
   
      return items.filter((item) =>
        item.types.some((type: any) =>
          type.type.name.toString().toLowerCase().includes(searchText)
        )
      );
    } else return items;
  }
}
