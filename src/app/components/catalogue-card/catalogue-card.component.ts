import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'comp-catalogue-card',
  templateUrl: './catalogue-card.component.html',
  styleUrls: ['./catalogue-card.component.css']
})
export class CatalogueCardComponent implements OnInit {
  @Input() pokemon?: Pokemon
  public isCaught: boolean = false
  constructor() { }

  ngOnInit(): void {
  }

  setCaught(value: boolean) {
    this.isCaught = value
  }
}
