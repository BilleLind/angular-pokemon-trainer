export interface PokemonDatails {
    id: number;
    name: string;
    types: TypeSlot[];
    abilities: AbilitySlot[];
    stats: StatSlot[];
    moves: MoveSlot[];
    height: string;
    weight: string;
}  

// types
export interface TypeSlot {
    type: Type;
}  

export interface Type {
    name: string;
}  

// abilities
export interface AbilitySlot {
    ability: Ability;
    slot: Ability;
}  

export interface Ability {
    name: string;
}  

// stats
export interface StatSlot {
    base_stat: Ability;
    stat: Stat;
}  

export interface Stat {
    name: string;
}

// Move
export interface MoveSlot {
    move: Move;
} 

export interface Move {
    name: string;
}  
