import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Location, NgIf } from '@angular/common';
import { Router } from '@angular/router';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from 'src/app/utils/storage.util';
import { environment } from 'src/environments/environment';

const { apiTrainers, apiKey } = environment;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  public currentRoute: string = '/'
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
  }

  constructor(
    private readonly trainerService: TrainerService,
    private router: Router,
    private location: Location,
    private readonly http: HttpClient
  ) {
   /*  this.currentRoute = location.path() */
    router.events.subscribe((val) => {
      this.currentRoute = location.path()
    })
  }

  logout() {
    StorageUtil.storageRemove(StorageKeys.Trainer);
    this.router.navigate(['/login']);
    location.reload();
  }


  // for testing. could be added as a functionality with a "are you sure" message
  /*deleteUser() {
    let trainer = this.trainerService.trainer
    const id = trainer?.id 

    const headers =  new HttpHeaders({
    "Content-Type": "application/json",
    "x-api-key": apiKey

    });
    this.http.delete(`${apiTrainers}/${id}`, { headers })
        .subscribe(() =>  console.log("logged out"));
    this.logout();     
  }*/
}
