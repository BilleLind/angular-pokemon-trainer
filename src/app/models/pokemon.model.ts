export interface Pokemon {
    id: number;
    name: string;
    types: TypeSlot[];
    abilities: AbilitySlot[];
    stats: StatSlot[];
}  

// types
export interface TypeSlot {
    type: Type;
}  

export interface Type {
    name: string;
}  

// abilities
export interface AbilitySlot {
    ability: Ability;
}  

export interface Ability {
    name: string;
}  

// stats
export interface StatSlot {
    base_stat: Ability;
    stat: Stat;
}  

export interface Stat {
    name: string;
} 