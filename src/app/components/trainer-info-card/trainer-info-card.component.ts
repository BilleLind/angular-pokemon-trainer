import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer-info-card',
  templateUrl: './trainer-info-card.component.html',
  styleUrls: ['./trainer-info-card.component.css']
})
export class TrainerInfoCardComponent implements OnInit {
 
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer
  }

  constructor(private readonly trainerService: TrainerService) { }

  ngOnInit(): void {
    this.trainer
  }

}
