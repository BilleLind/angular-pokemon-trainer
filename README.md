# This is our Pokemon Trainer application created with Angular and styled with Bootstrap 5

Site Url: https://angular-pokemon-trainer.netlify.app

<h2> Table of contects </h2>

- Background
- Install
- Usage
- Maintainers
- License
- Authors and acknowledgment


<h2> Background </h2>
This is our Pokemon Trainer application created with Angular and styled with Bootstrap 5 
 

<h2> Install </h2>

- NPM/Node.js (LTS – Long Term Support version)
- Angular CLI
- Visual Studio Code Text Editor/ IntelliJ
- git

<h2> Usage </h2>
At the first page you can login by typing a username. after pressing the button you will be taking to the catalogue page where see Pokemon which you could cath and release or click for more info or go to the trainer page. At the trainer page you can see how much pokemon you have and release them. 


<h2> Maintainers </h2>
@trygvejohannessen88, @BilleLind and @bartomen

<h2> License </h2>
The project is open-source. Feel free to use it in your own projects, as long as credit the work.

<h2> Authors and Acknowledgmen </h2>
Code author: Anders Bille Lind, Trygve Johannessen, Bart van Dongen.

Assignment given by: Piotr Dziubinski, Lecturer at Noroff University College.
