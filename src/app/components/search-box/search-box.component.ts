import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { FilterPipe } from 'src/app/utils/filter.pipe';
@Component({
  selector: 'comp-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css'],
})
export class SearchBoxComponent implements OnInit {
  @Input() pokemonList: Pokemon[] = [];
  @Output() filteredPokemonsEvent = new EventEmitter<Pokemon[]>();

  /* search: SearchQuery = {
    type: '',
    query: ''
  } */
  query: string = '';
  selectedValue: string = 'name';

  constructor(private filterPipe: FilterPipe) {}

  ngOnInit(): void {}

  searchChange(obj:any){
    this.filteredPokemonsEvent.emit(this.filterPipe.transform(this.pokemonList, this.query, this.selectedValue))
  }
  
}
