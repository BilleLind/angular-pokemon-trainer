import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PokemonDatails } from '../models/pokemon.details.model';

const { apiPokemon, apiKey, apiTrainers } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailsService {
  private _pokemonList: PokemonDatails[] = [];
 
  get pokemonList(): PokemonDatails[] {
    return this._pokemonList;
  }

  constructor(private readonly http: HttpClient) {}

  public findPokemon(id: number) {
    this.http.get<PokemonDatails>(`${apiPokemon}/${id}`)
      .subscribe((pokemon: PokemonDatails) => {
        this._pokemonList.push(pokemon)
      })
  }

}
