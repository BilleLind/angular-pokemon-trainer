import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';


const { apiTrainers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Dependecy Injection.
  constructor(private readonly http: HttpClient) { }

  // > Models, httpClient, observables, and RxJs operators
public login(username: string): Observable<Trainer>{
  return this.checkUsername(username)
  .pipe(
    switchMap((trainer: Trainer | undefined) => {
        if (trainer === undefined) { // trainer does not exist
          return this.createTrainer(username);
        }
        return of(trainer);
      })
  )
}
  // Login

  // Check if user exits
private checkUsername(username: string): Observable<Trainer | undefined> {
  return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`)
    .pipe(
      // Rxjs operators
      map ((response:Trainer[])=>response.pop())
    )  
} 
  // IF NOT user - Create a User
  private createTrainer(username:string): Observable<Trainer>{
    
    const trainer = {
      username,
      pokemon:[]
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });

    return this.http.post<Trainer>(apiTrainers, trainer, {headers})
  }

  // if user || Created User -> Store user

}
