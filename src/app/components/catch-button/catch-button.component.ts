import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css'],
})
export class CatchButtonComponent implements OnInit {
  public isLoading: boolean = false;
  public isCaught: boolean = false;
  @Input() pokemonId: number = 0;

  @Output() isCaughtEmitter = new EventEmitter<boolean>();

  constructor(
    private trainerService: TrainerService,
    private pokemonService: PokemonService
  ) {}

  ngOnInit(): void {
    this.isCaught = this.trainerService.inFavourites(this.pokemonId);
    this.setCaught()
  }

  onCaughtClick(): void {
    this.isLoading = true;

    this.pokemonService.addToCaught(this.pokemonId).subscribe({
      next: (response: Trainer) => {
        this.isLoading = false;
        this.isCaught = this.trainerService.inFavourites(this.pokemonId);
        this.setCaught();
      },
      error: (error: HttpErrorResponse) => {
        console.error('ERROR', error.message);
      },
    });
  }

  setCaught() {
    this.isCaughtEmitter.emit(this.isCaught);
  }
}
