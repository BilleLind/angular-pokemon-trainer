import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonDatails } from 'src/app/models/pokemon.details.model';
import { PokemonDetailsService } from 'src/app/services/pokemon.details.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.css']
})
export class DetailsPage implements OnInit {
  id: number = 0;

  get pokemonList(): PokemonDatails[] {
    console.log(this.pokemonDetailsService.pokemonList)
    return this.pokemonDetailsService.pokemonList;
  }

  constructor(
    private pokemonDetailsService: PokemonDetailsService,
    private route: ActivatedRoute
  ) {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit(): void {
    this.pokemonDetailsService.findPokemon(this.id)
  }

}
