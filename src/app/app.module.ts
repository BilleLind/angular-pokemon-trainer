import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { SearchBoxComponent } from './components/search-box/search-box.component';
import { CatalogueCardComponent } from './components/catalogue-card/catalogue-card.component';
import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { DetailsPage } from './pages/details/details.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { NavbarComponent } from './components/navbar/navbar.component';

import { FilterPipe } from './utils/filter.pipe';
import { CatchButtonComponent } from './components/catch-button/catch-button.component';
import { PageNotFoundPage } from './pages/page-not-found/page-not-found.page';
import { TrainerInfoCardComponent } from './components/trainer-info-card/trainer-info-card.component';

@NgModule({
  declarations: [ // Componets
    AppComponent,
    SearchBoxComponent,
    CatalogueCardComponent,
    NavbarComponent,
    LoginPage,
    TrainerPage,
    CataloguePage,
    DetailsPage,
    LoginFormComponent,
    CatchButtonComponent,
    PageNotFoundPage,
    TrainerInfoCardComponent
  ],
  imports: [ // Modules!
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [FilterPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
