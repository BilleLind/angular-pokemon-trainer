import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit {
  filteredPokemonList: Pokemon[] = this.pokemonList

  get pokemonList(): Pokemon[] {
    return this.pokemonService.pokemonList;
  }

  constructor(
    private pokemonService: PokemonService,
  ) { }

  ngOnInit(): void {
    this.pokemonService.findAllPokemon()
  }

  setFiltered(filered: Pokemon[]) {
    this.filteredPokemonList = filered
  }

}
